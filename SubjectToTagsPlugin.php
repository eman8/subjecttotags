<?php

/*
 * SubjectToTags Plugin
 *
 * Synchronizes Omeka tags with DC.Subject entries
 *
 */

class SubjectToTagsPlugin extends Omeka_Plugin_AbstractPlugin
{

  protected $_hooks = array(
  	'items_batch_edit_custom',
    'admin_items_batch_edit_form',
    'after_save_item',
  );

    /**
     * Add custom fields to the item batch edit form.
     */
  public function hookAdminItemsBatchEditForm()
  {
    $view = get_view();
    $db = get_db();
    $user = current_user();

    if (in_array($user->role, ['super', 'admin'])) : ?>

      <fieldset id="subject-to-tags">
          <h2><?php echo __('Subject to Tags'); ?></h2>
              <div class="inputs five columns omega">
                  <?php echo $view->formCheckbox(
                      'custom[subjecttotags]',
                      null,
                      array(
                          'checked' => false,
                          'class' => 'subject-to-tags-checkbox',
                  )); ?>
                  <span class="explanation">
                    <?php echo 'Delete ALL unused tags '; ?>
                  </span>
              </div>
      </fieldset>
  <?php
      endif;
  }

  function hookItemsBatchEditCustom ($args)
  {
    $item = $args['item'];
    $custom = $args['custom'];
    $db = get_db();
    if ($args['custom']['subjecttotags']) {
      // Delete all unused tags
      $delete = $db->query("DELETE FROM `$db->Tags` WHERE id NOT IN (SELECT DISTINCT(tag_id) FROM omeka_records_tags)")->fetch();
    }
  }

  function hookAfterSaveItem($args)
  {
  	// Add tags based on DC.Subject
  	$litem = $args['record'];
    $tags = metadata($litem, array('Dublin Core', 'Subject'), array('all' => true));
    $itemId = metadata($litem, 'id');

  	$db = get_db();
  	$tagsIds = array();
    foreach ($tags as $id => $tag) {
    	// Create tag if it doesn't exist
    	$tag = trim(str_replace("'", "''", $tag));
    	$tagId = $db->query("SELECT id FROM `$db->Tags` WHERE name = '$tag'")->fetch();
    	if (empty($tagId)) {
      	$tag = substr($tag, 0, 255);
    		$db->query("INSERT INTO `$db->Tags` (name) VALUES(?)", [$tag]);
    		$tagId['id'] = $db->getAdapter()->lastInsertId();
    	}
    	$tagsIds[$tag] = $tagId['id'];
    }
    $t = serialize($tagsIds);

    // Delete all item tags
    $delete = $db->query("DELETE FROM `$db->RecordsTags` WHERE record_id = $itemId AND record_type='Item'")->fetch();

    // Tag item with choosen tags
    foreach ($tagsIds as $name => $id) {
    	$query = "INSERT INTO `$db->RecordsTags` (record_id, record_type, tag_id) VALUES ($itemId, 'Item', $id)";
			$tagged = $db->query("INSERT INTO `$db->RecordsTags` (record_id, record_type, tag_id) VALUES ($itemId, 'Item', $id)")->fetch();
    }
  }
}
