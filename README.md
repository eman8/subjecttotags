# SubjectToTags
Synchronize Omeka tags with DC.Subject entries

Note : When this plugin is enabled, it will subside anything you type in the Tags tab on the Item edition page (i.e : you can't create tags the usual way).
